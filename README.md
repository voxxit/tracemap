# tracemap

`tracemap` runs `traceroute -n` against a given `host`, and create a static Google Map for the resulting network coordinates.

First, you need to download the free `GeoLite2-City.mmdb` database from MaxMind. If you have the more accurate `GeoIP2-City.mmdb` database, you can use the `db` environment variable to specify it's location on your file system:

```
./bin/setup

host=google.com ./tracemap
```

To use your own database:

```
./bin/setup

host=google.com db=/var/lib/maxmind/GeoIP2-City.mmdb ./tracemap
```
